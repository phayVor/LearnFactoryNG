import React, { Component } from 'react';
import logo from '../logo.svg';
import '../styles/App.css';
import Content from './Content';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: 0
    }
  }

  setNewNumber = () => {
    this.setState({data: this.state.data + 1 })
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">LifeCycle is a go</h1>
        </header>
        <div className="App-intro">
          <button onClick={this.setNewNumber}>Increment</button>
          <Content myNumber={this.state.data}>set data</Content>
        </div>
      </div>
    );
  }
}

export default App;
