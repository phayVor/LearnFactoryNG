import React, { Component } from 'react';

export default class Content extends Component {
  componentWillMount() {
    console.log('component will mount!')
  }
  componentDidMount() {
    console.log('component did mount!')
  }
  componentWillReceiveProps(newProps) {
    console.log('component will recieve props')
  }
  shouldComponentUpdate(newProps, newState) {
    return true;
  }
  componentWillUpdate(nextProps, nextState) {
    console.log('component will update');
  }
  componentDidUpdate(prevProps, prevState) {
    console.log('component did update');
  }
  render() {
    return (
      <div>
        <h3>{this.props.myNumber}</h3>
      </div>
    );
  }
}
